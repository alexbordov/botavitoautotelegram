import os

import telebot

PROXY_FILE = 'proxy.txt'
PROXY_LIST = []
BOT_TOKEN = '***************'
CHANNEL_NAME = '@111111111'
SINGLE_RUN = True
FILE_LOG = 'bot_log.log'
WITH_PROXY = 0
bot = telebot.TeleBot(BOT_TOKEN)


def get_proxy():
    with open(PROXY_FILE) as f:
        proxy_l = f.readlines()
    for i in proxy_l:
        PROXY_LIST.append(f'https://{i.strip()}')


def clear_log():
    if os.path.getsize(FILE_LOG) > 10 ** 6:
        with open(FILE_LOG, "w") as f:
            f.write("Log clears")
