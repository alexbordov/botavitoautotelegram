import Post


class TelegramBot:
    def send_new_posts(self, item: Post):
        message = f"""Новое объявление!\nТип: {item.type_url.type_url}\nНазвание: {item.title}\nМестоположение: {item.city}\nЦена: {item.price}\nОписание: {
        item.desc}\nТелефон: {item.phone}\nИмя: {item.user_name}\nДата последнего обновления: {item.data}\nСсылка: {item.url} """
        print(message)
        # Tools.bot.send_message(chat_id=Tools.CHANNEL_NAME, text=message, parse_mode='HTML')
