import logging
import time

import Avito
import Tools


def main():
    logging.getLogger('requests').setLevel(logging.CRITICAL)
    logging.basicConfig(format='[%(asctime)s] %(filename)s:%(lineno)d %(levelname)s - %(message)s', level=logging.INFO,
                        filename=Tools.FILE_LOG, datefmt='%d.%m.%Y %H:%M:%S')
    try:
        # get_proxy()
        pass
    except Exception as e:
        logging.error('Exception of type {0!s} in get_proxy(): {1}'.format(type(e).__name__, e))
    if not Tools.SINGLE_RUN:
        while True:
            Tools.clear_log()
            avt = Avito.Avito()
            avt.check_new_posts_avito()
            # auto = Auto.Auto()
            # auto.check_new_posts_auto()
            logging.info('[App] Script went to sleep.')
            time.sleep(60 * 10)
    else:
        Tools.clear_log()
        avt = Avito.Avito()
        avt.check_new_posts_avito()
        # auto = Auto.Auto()
        # auto.check_new_posts_auto()
    logging.info('[App] Script exited.\n')


if __name__ == '__main__':
    main()
